package sample;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;


public class Controller implements Initializable {
    @FXML
    private TableColumn<ImageProcessingJob, String> imageNameColumn;
    @FXML
    private TableColumn<ImageProcessingJob, Double> progressColumn;
    @FXML
    private TableColumn<ImageProcessingJob, String> statusColumn;
    @FXML
    private TableView<ImageProcessingJob> imagesTable;
    @FXML
    private Button fileSelection;
    @FXML
    private Button directionSelection;
    @FXML
    private Button executeButton;
    @FXML
    private Button threadsButton;
    @FXML
    private ChoiceBox threadsList;
    @FXML
    private Label directionLabel;
    @FXML
    private Label timeLabel;

    private File outputDir;
    private ObservableList<ImageProcessingJob> jobs;
    private ExecutorService executor;
    private Alert alert;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        imageNameColumn.setCellValueFactory(
                p -> new SimpleStringProperty(p.getValue().getOriginalFile().getName()));
        statusColumn.setCellValueFactory(
                p -> p.getValue().statusProperty());
        progressColumn.setCellFactory(
                ProgressBarTableCell.<ImageProcessingJob>forTableColumn());
        progressColumn.setCellValueFactory(
                p -> p.getValue().progressProperty().asObject());

        alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");

        jobs = FXCollections.observableList(new ArrayList<>());
        executor = Executors.newSingleThreadExecutor();
        buttonDisabler(true);
    }

    private void buttonDisabler(boolean disable) {
        directionSelection.setDisable(disable);
        executeButton.setDisable(disable);
        threadsList.setDisable(disable);
        threadsButton.setDisable(disable);
        directionLabel.setVisible(!disable);
    }

    @FXML
    private void selectFiles() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("JPEGs", "*.jpg"));
        List<File> selectedFiles = fileChooser.showOpenMultipleDialog(null);
        if (selectedFiles == null || selectedFiles.isEmpty()) {
            alert.setHeaderText("You have not chosen any file!");
            alert.setContentText("Please select proper file to proceed.");
            alert.showAndWait();
            buttonDisabler(true);
            jobs.clear();
        } else {
            buttonDisabler(false);
            executeButton.setDisable(true);
            jobs.clear();
            selectedFiles.stream().forEach(item -> jobs.add(new ImageProcessingJob(item)));
            imagesTable.setItems(jobs);
        }
    }

    @FXML
    private void selectDirectory() {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        outputDir = directoryChooser.showDialog(null);
        if (outputDir == null) {
            directionLabel.setText("No direction selected");
            executeButton.setDisable(true);
            alert.setHeaderText("You have not chosen any directory!");
            alert.setContentText("Please select proper folder to proceed.");
            alert.showAndWait();
        } else {
            directionLabel.setText(outputDir.getAbsolutePath());
            executeButton.setDisable(false);
            jobs.stream().forEach(imageProcessingJob ->
                    imageProcessingJob.setOutputDir(outputDir));
        }
    }

    @FXML
    private void selectThreads() {
        if (threadsList.getSelectionModel().getSelectedItem() != null) {
            int selection = threadsList.getSelectionModel().getSelectedIndex();
            if (selection == 0) {
                executor = Executors.newSingleThreadExecutor();
            } else if (selection == 1) {
                executor = new ForkJoinPool();
            } else if (selection == 2) {
                executor = new ForkJoinPool(2);
            } else if (selection == 3) {
                executor = new ForkJoinPool(4);
            } else if (selection == 4) {
                executor = new ForkJoinPool(8);
            }
        } else {
            alert.setHeaderText("You have not chosen any thread model!");
            alert.setContentText("Please select proper value to proceed.");
            alert.showAndWait();
        }
    }

    @FXML
    private void execute() {
        new Thread(this::processFiles).start();
    }

    @FXML
    private void processFiles() {
        Platform.runLater(() -> {
            buttonDisabler(true);
            fileSelection.setDisable(true);
        });
        long startTime = System.currentTimeMillis();
        try {
            jobs.stream().forEach(job -> executor.submit(job));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            executor.shutdown();
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
            Platform.runLater(() -> {
                directionLabel.setText("No direction selected");
                fileSelection.setDisable(false);
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        long endTime = System.currentTimeMillis();
        Platform.runLater(() -> {
            timeLabel.setText("Last execution took " + (double) (endTime - startTime) / 1000 + "s");
            threadsList.setValue("Single Thread");
            executor = Executors.newSingleThreadExecutor();
        });
    }
}
