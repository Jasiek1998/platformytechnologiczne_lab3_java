package sample;

import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImageProcessingJob implements Runnable {
    private File originalFile; //oryginalny plik graficzny
    private File outputDir; //katalog docelowy
    private SimpleStringProperty status; //status pliku
    private DoubleProperty progress; //progres wykonywanych operacji

    File getOriginalFile() {
        return originalFile;
    }

    SimpleStringProperty statusProperty() {
        return status;
    }

    DoubleProperty progressProperty() {
        return progress;
    }

    void setOutputDir(File outputDir) {
        this.outputDir = outputDir;
    }

    ImageProcessingJob(File originalFile) {
        this.originalFile = originalFile;
        this.status = new SimpleStringProperty("Waiting...");
        this.progress = new SimpleDoubleProperty(0);
    }

    @Override
    public void run() {
        convertToGrayscale(originalFile, outputDir, progress);
    }


    private void convertToGrayscale(
            File originalFile, //oryginalny plik graficzny
            File outputDir, //katalog docelowy
            DoubleProperty progressProperty//własność określająca postęp operacji
    ) {
        try {
            long startTime = System.currentTimeMillis();
            //wczytanie oryginalnego pliku do pamięci
            BufferedImage original = ImageIO.read(originalFile);

            //przygotowanie bufora na grafikę w skali szarości
            BufferedImage grayscale = new BufferedImage(
                    original.getWidth(), original.getHeight(), original.getType());
            //przetwarzanie piksel po pikselu
            Platform.runLater(() -> status.set("Processing..."));
            for (int i = 0; i < original.getWidth(); i++) {
                for (int j = 0; j < original.getHeight(); j++) {
                    //pobranie składowych RGB
                    int red = new Color(original.getRGB(i, j)).getRed();
                    int green = new Color(original.getRGB(i, j)).getGreen();
                    int blue = new Color(original.getRGB(i, j)).getBlue();
                    //obliczenie jasności piksela dla obrazu w skali szarości
                    int luminosity = (int) (0.21 * red + 0.71 * green + 0.07 * blue);
                    //przygotowanie wartości koloru w oparciu o obliczoną jaskość
                    int newPixel =
                            new Color(luminosity, luminosity, luminosity).getRGB();
                    //zapisanie nowego piksela w buforze
                    grayscale.setRGB(i, j, newPixel);
                }
                //obliczenie postępu przetwarzania jako liczby z przedziału [0, 1]
                double processed = (1.0 + i) / original.getWidth();
                Thread.sleep(2);
                //aktualizacja własności zbindowanej z paskiem postępu w tabeli
                Platform.runLater(() -> progressProperty.set(processed));
            }
            //przygotowanie ścieżki wskazującej na plik wynikowy
            Path outputPath = Paths.get(outputDir.getAbsolutePath(), originalFile.getName());

            //zapisanie zawartości bufora do pliku na dysku
            ImageIO.write(grayscale, "jpg", outputPath.toFile());
            long endTime = System.currentTimeMillis();
            Platform.runLater(() -> status.set("Finished converting " + originalFile.getName() + " in " + (double) (endTime - startTime) / 1000 + "s"));
        } catch (IOException | InterruptedException ex) {
            //translacja wyjątku
            throw new RuntimeException(ex);
        }
    }
}
